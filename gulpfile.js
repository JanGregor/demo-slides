var gulp = require('gulp');

var pug    = require('gulp-pug');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass   = require('gulp-sass');
var copy   = require('gulp-copy');


gulp.task('html', function buildHTML() {
  return gulp.src('./views/*.pug')
    .pipe(pug({}))
    .pipe(gulp.dest('./build'));
})

gulp.task('js', function builJavaScript() {
  var files = [
    './node_modules/reveal.js/js/reveal.js',
    './src/main.js'
  ];

  return gulp.src(files)
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build'));
})

gulp.task('css', function () {
  var files = [
    './node_modules/reveal.js/css/reveal.css',
    './node_modules/reveal.js/css/theme/black.css',
    './layout/main.scss'
  ];

  return gulp.src(files)
    .pipe(concat('main.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./build'));
});

gulp.task('fonts', function () {
  var files = [
    './node_modules/reveal.js/lib/**/*'
  ];

  return gulp
    .src(files)
    .pipe(copy('./build', {
      prefix: 2
    }));
});

gulp.task('default', ['html', 'js', 'css', 'fonts']);